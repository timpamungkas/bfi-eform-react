import React from "react"
import {
  Card,
  CardBody,
  Row,
  Col,
} from "reactstrap"

import loginImg from "../../../../assets/img/pages/login-bfi.png"
import "../../../../assets/scss/pages/authentication.scss"

import LoginJWT from "./LoginJWT"

class Login extends React.Component {
  state = {
    activeTab: "1",
    username : "",
    password: ""
  }
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }
  render() {
    return (
      <Row className="m-0 justify-content-center">
        <Col
          sm="8"
          xl="7"
          lg="10"
          md="8"
          className="d-flex justify-content-center"
        >
          <Card className="bg-authentication login-card rounded-0 mb-0 w-100">
            <Row className="m-0">
              <Col
                lg="6"
                className="d-lg-block d-none text-center align-self-center px-1 py-0"
              >
                <img src={loginImg} alt="loginImg" />
              </Col>
              <Col lg="6" md="12" className="p-0">
                <Card className="rounded-0 mb-0 px-2">
                      <CardBody>
                        <h3 className="primary">BFI Finance</h3>
                        <p>Please login to your account.</p>
                        <LoginJWT />
                      </CardBody>
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    )
  }
}
export default Login
