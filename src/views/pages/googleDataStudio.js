import React from "react"
import Iframe from 'react-iframe'

class GoogleDataStudio extends React.Component{
  render(){
    return <Iframe url="https://datastudio.google.com/embed/reporting/f5552ef1-2e11-48ad-8b2c-46770cecd1d6/page/m7JTB"
    width="950px"
    height="450px"
    id="myId"
    className="myClassname"
    display="initial"
    position="relative"/>
  }
}

export default GoogleDataStudio