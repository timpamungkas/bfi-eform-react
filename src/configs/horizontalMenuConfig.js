import React from "react"
import * as Icon from "react-feather"

const horizontalMenuConfig = [
  {
    id: "home",
    title: "Home",
    type: "item",
    icon: <Icon.Home size={20} />,
    permissions: ["manager", "staff"],
    navLink: "/home"
  },
  {
    id: "digital-form",
    title: "Digital Form",
    type: "dropdown",
    icon: <Icon.FileText size={20} />,
    children: [
      {
        id: "manager",
        title: "Manager",
        type: "dropdown",
        icon: <Icon.Folder size={10} />,
        permissions: ["manager"],
        children: [
          {
            id: "manager01",
            title: "Manager01",
            type: "item",
            icon: <Icon.Circle size={10} />,
            permissions: ["manager"],
            navLink: "/manager01"
          }, {
            id: "manager02",
            title: "Manager02",
            type: "item",
            icon: <Icon.Circle size={10} />,
            permissions: ["manager"],
            navLink: "/manager02"
          }
        ]
      },
      {
        id: "staff",
        title: "Staff",
        type: "dropdown",
        icon: <Icon.Folder size={10} />,
        permissions: ["staff"],
        children: [
          {
            id: "staff01",
            title: "Staff01",
            type: "item",
            icon: <Icon.Circle size={10} />,
            permissions: ["staff"],
            navLink: "/staff01"
          }, {
            id: "staff02",
            title: "Staff02",
            type: "item",
            icon: <Icon.Circle size={10} />,
            permissions: ["staff"],
            navLink: "/staff02"
          }
        ]
      }
    ]
  }
]

export default horizontalMenuConfig
