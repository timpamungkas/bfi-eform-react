
import React from "react"
import * as Icon from "react-feather"
const navigationConfig = [
    {
    id: "home",
    type: "item",
    icon: <Icon.Home size={20} />,
    navLink: "/home",
    title: "Home"
  }
,
    {
    id: "manager",
    type: "collapse",
    icon: <Icon.Archive size={20} />,
    permissions: ["manager"],
    title: "Manager",
  children: [
  ]
  }
,
    {
    id: "staff",
    type: "collapse",
    icon: <Icon.Folder size={20} />,
    permissions: ["staff"],
    title: "Staff",
  children: [
      {
    id: "staff01",
    type: "item",
    icon: <Icon.Circle size={10} />,
    navLink: "/staff01",
    title: "Staff01"
  }
,
      {
    id: "staff02",
    type: "item",
    icon: <Icon.Circle size={10} />,
    navLink: "/staff02",
    title: "Staff02"
  }

  ]
  }

]

export default navigationConfig

