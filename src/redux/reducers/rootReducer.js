import { combineReducers } from "redux"
import calenderReducer from "./calendar/"
import customizer from "./customizer/"
import auth from "./auth/"
import navbar from "./navbar/Index"

const rootReducer = combineReducers({
  calendar: calenderReducer,
  customizer: customizer,
  auth: auth,
  navbar: navbar
})

export default rootReducer
