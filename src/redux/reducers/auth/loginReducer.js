export const login = (state = { userRole: "default" }, action) => {
  switch (action.type) {
    case "LOGIN_WITH_JWT": {
      state.userRole = action.payload.loggedInUser.role
      return { ...state, values: action.payload }
    }
    case "LOGOUT_WITH_JWT": {
      state.userRole = "default"
      return { ...state, values: action.payload }
    }
    default: {
      return state
    }
  }
}
