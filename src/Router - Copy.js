import React, { Suspense, lazy } from "react"
import { Router, Switch, Route } from "react-router-dom"
import { history } from "./history"
import { connect } from "react-redux"
import Spinner from "./components/@vuexy/spinner/Loading-spinner"
import { ContextLayout } from "./utility/context/Layout"


const Login = lazy(() =>
  import("./views/pages/authentication/login/Login")
)
const Home = lazy(() =>
  import("./views/apps/calendar/Calendar")
)
const Staff02 = lazy(() =>
  import("./views/pages/staff02")
)
const Manager02 = lazy(() =>
  import("./views/pages/manager02")
)
const Manager01 = lazy(() =>
  import("./views/pages/manager01")
)
const Staff01 = lazy(() =>
  import("./views/pages/staff01")
)

// Set Layout and Component Using App Route
// display login screen if not authenticated
const RouteConfig = ({
  component: Component,
  fullLayout,
  user,
  auth,
  ...rest
}) => (
    <Route
      {...rest}
      render={props => {
        return (
          <ContextLayout.Consumer>
            {
              context => {
                let LayoutTag =
                  fullLayout === true
                    ? context.fullLayout
                    : context.state.activeLayout === 'horizontal'
                      ? context.horizontalLayout
                      : context.VerticalLayout

                return auth.values !== undefined && auth.values.loggedInUser.isSignedIn ? (
                  <LayoutTag {...props} permission='{user}'>
                    <Suspense fallback={<Spinner />}>
                      <Component {...props}></Component>
                    </Suspense>
                  </LayoutTag>
                ) : (
                    <context.fullLayout {...props} permission={user}>
                      <Suspense fallback={<Spinner />}>
                        <Login {...props} />
                      </Suspense>
                    </context.fullLayout>
                  )
              }
            }
          </ContextLayout.Consumer>
        )
      }}
    />
  )
const mapStateToProps = state => {
  return {
    user: state.auth.login.userRole,
    auth: state.auth.login
  }
}

const AppRoute = connect(mapStateToProps)(RouteConfig)

class AppRouter extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <AppRoute 
            exact path="/" component={Login}  />
          <AppRoute 
            exact path="/home" component={Home}  />
          <AppRoute 
            exact path="/staff02" component={Staff02}  />
          <AppRoute 
            exact path="/manager02" component={Manager02}  />
          <AppRoute 
            exact path="/manager01" component={Manager01}  />
          <AppRoute 
            exact path="/staff01" component={Staff01}  />
        </Switch>
      </Router>
    )
  }
}

export default AppRouter
